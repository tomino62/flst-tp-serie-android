package fr.cichocki.tpseries.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import fr.cichocki.tpseries.model.Serie;

/**
 * Created by tcichock on 18/02/2016.
 */
public class CompareSerieList implements Comparator<Serie> {

    public static boolean ListContainsTitle(List<Serie> list, String title) {
        for (Serie serie : list) {
            if (serie.getTitle().equalsIgnoreCase(title)) {
                return true;
            }
        }
        return false;
    }

    public static int getPositionByTitle(List<Serie> list, String title) {

        for (Serie serie : list) {
            if (serie.getTitle().equalsIgnoreCase(title)) {
                return list.indexOf(serie);
            }
        }
        return 0;
    }

    public static void removeByTitle(List<Serie> list, String title) {

        Iterator<Serie> it = list.iterator();
        while (it.hasNext()) {
            Serie serie = it.next();
            if (serie.getTitle().equalsIgnoreCase(title)) {
                it.remove();
            }
        }
    }


    @Override
    public int compare(Serie serie1, Serie serie2) {
        return serie1.getTitle().compareTo(serie2.getTitle());
    }

}
