package fr.cichocki.tpseries.util;

import java.util.Comparator;

import fr.cichocki.tpseries.model.Serie;

/**
 * Created by tcichock on 18/02/2016.
 */
public class SerieRatingCompare implements Comparator<Serie> {

    @Override
    public int compare(Serie serie1, Serie serie2) {
        String rate1 = serie1.getImdbRating() ;
        Float rateFloat1 = Float.valueOf(rate1);

        String rate2 = serie2.getImdbRating() ;
        Float rateFloat2 = Float.valueOf(rate2);


        return rateFloat2.compareTo(rateFloat1);
    }

}
