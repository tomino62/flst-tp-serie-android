package fr.cichocki.tpseries;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import fr.cichocki.tpseries.adapter.SeriePagerAdapter;
import fr.cichocki.tpseries.factory.SerieFactory;
import fr.cichocki.tpseries.model.BestSerieListSingleton;
import fr.cichocki.tpseries.model.FavoriteSerieListSingleton;
import fr.cichocki.tpseries.model.Serie;
import fr.cichocki.tpseries.model.AllSerieListSingleton;
import fr.cichocki.tpseries.ui.fragments.SerieDetailActivity;
import fr.cichocki.tpseries.ui.fragments.SerieListFragment;
import fr.cichocki.tpseries.util.CompareSerieList;
import fr.cichocki.tpseries.util.SerieRatingCompare;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SeriePagerAdapter mPagerAdapter;
    private static SerieFactory serieFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.viewpager);


        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        /**
        *
        *  Gestion des Singletons, si c'est le premier appel, on instencie les singletons
         * Tout ca pour la gestion des favoris
        **/
        if(AllSerieListSingleton.getInstance().getSerieList().isEmpty()){
            AllSerieListSingleton.getInstance().setSerieList(SerieFactory.getSerieList());
        }

        if(BestSerieListSingleton.getInstance().getSerieList().isEmpty()){
            BestSerieListSingleton.getInstance().setSerieList(SerieFactory.getBestSeriesList());
        }

        if(FavoriteSerieListSingleton.getInstance().getSerieList().isEmpty()) {
            FavoriteSerieListSingleton.getInstance().setSerieList(SerieFactory.getFavoriteSeriesList());
        }


        // Création de la liste de Fragments que fera défiler le SeriePagerAdapter
        List fragments = new Vector();
        Bundle args = new Bundle();
        Bundle args2 = new Bundle();
        Bundle args3 = new Bundle();


        // Ajout des Fragments dans la liste avec argument pour savoir quelle liste afficher
        args.putSerializable("list", "all");
        fragments.add(Fragment.instantiate(this, SerieListFragment.class.getName(), args));
        args2.putSerializable("list", "best");
        fragments.add(Fragment.instantiate(this, SerieListFragment.class.getName(), args2));
        args3.putSerializable("list", "favorite");
        fragments.add(Fragment.instantiate(this, SerieListFragment.class.getName(), args3));

        // Création de l'adapter qui s'occupera de l'affichage de la liste de  Fragments
        this.mPagerAdapter = new SeriePagerAdapter(super.getSupportFragmentManager(), fragments);

        ViewPager pager = (ViewPager) super.findViewById(R.id.viewpager);
        // Affectation de l'adapter au ViewPager
        pager.setAdapter(this.mPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_random) {

            List<Serie> list = AllSerieListSingleton.getInstance().getSerieList();
            Random randomGenerator = new Random() ;
            int position = randomGenerator.nextInt(list.size());

            Intent intent = new Intent(this, SerieDetailActivity.class);
            intent.putExtra("serie", list.get(position));
            startActivity(intent);

        } else if (id == R.id.nav_alphabetic) {

            List<Serie> allList         = AllSerieListSingleton.getInstance().getSerieList();
            List<Serie> bestRatingList  = BestSerieListSingleton.getInstance().getSerieList();
            List<Serie> favoritesList   = FavoriteSerieListSingleton.getInstance().getSerieList();

            Collections.sort(allList, new CompareSerieList());
            AllSerieListSingleton.getInstance().setSerieList(allList);

            Collections.sort(bestRatingList, new CompareSerieList());
            BestSerieListSingleton.getInstance().setSerieList(bestRatingList);

            Collections.sort(favoritesList, new CompareSerieList());
            FavoriteSerieListSingleton.getInstance().setSerieList(favoritesList);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_rating_order) {

            List<Serie> allList         = AllSerieListSingleton.getInstance().getSerieList();
            List<Serie> bestRatingList  = BestSerieListSingleton.getInstance().getSerieList();
            List<Serie> favoritesList   = FavoriteSerieListSingleton.getInstance().getSerieList();

            Collections.sort(allList, new SerieRatingCompare());
            AllSerieListSingleton.getInstance().setSerieList(allList);

            Collections.sort(bestRatingList, new SerieRatingCompare());
            BestSerieListSingleton.getInstance().setSerieList(bestRatingList);

            Collections.sort(favoritesList, new SerieRatingCompare());
            FavoriteSerieListSingleton.getInstance().setSerieList(favoritesList);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
