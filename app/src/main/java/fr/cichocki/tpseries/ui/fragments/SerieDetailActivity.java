package fr.cichocki.tpseries.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import fr.cichocki.tpseries.MainActivity;
import fr.cichocki.tpseries.R;
import fr.cichocki.tpseries.model.Serie;

public class SerieDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_serie_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_serie_details);
        setSupportActionBar(toolbar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Serie serie = (Serie) getIntent().getExtras().getSerializable("serie");

       if (savedInstanceState == null) {
            SerieDetailFragment fragment = SerieDetailFragment.newInstance(serie);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.serie_detail_container, fragment);
            ft.commit();
        }

    }

    @Override
    public void onBackPressed (){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


}
