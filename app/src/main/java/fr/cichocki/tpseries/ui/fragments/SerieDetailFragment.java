package fr.cichocki.tpseries.ui.fragments;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.List;

import fr.cichocki.tpseries.R;
import fr.cichocki.tpseries.factory.SerieFactory;
import fr.cichocki.tpseries.model.BestSerieListSingleton;
import fr.cichocki.tpseries.model.FavoriteSerieListSingleton;
import fr.cichocki.tpseries.model.Serie;
import fr.cichocki.tpseries.model.AllSerieListSingleton;
import fr.cichocki.tpseries.util.CompareSerieList;

/**
 * Created by tcichock on 16/02/2016.
 */
public class SerieDetailFragment extends Fragment {

    private static final String SERIE = "serie";
    private static final String POSITION = "position";

    private static SerieFactory serieFactory;

    private Serie mSerie;

    public SerieDetailFragment() {    }

    public static SerieDetailFragment newInstance(Serie serie) {
        SerieDetailFragment fragment = new SerieDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(SERIE, serie);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSerie = (Serie) getArguments().getSerializable(SERIE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        container.removeAllViews();

        final View rootView         = inflater.inflate(R.layout.activity_serie_detail, container, false);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar_serie_details);
        toolbar.setTitle((getActivity()).getTitle());

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView titleTextView      = (TextView) rootView.findViewById(R.id.serieDetailTitle);
        TextView plotTextView       = (TextView) rootView.findViewById(R.id.SerieDetailPlot);
        ImageView imageImageView    = (ImageView) rootView.findViewById(R.id.SerieDetailIcon);
        TextView yearTextView       = (TextView) rootView.findViewById(R.id.SerieDetailYear);
        TextView directorTextView   = (TextView) rootView.findViewById(R.id.SerieDetailDirector);
        TextView genreTextView      = (TextView) rootView.findViewById(R.id.SerieDetailGenre);
        TextView actorsTextView     = (TextView) rootView.findViewById(R.id.SerieDetailActors);
        RatingBar ratingBar         = (RatingBar) rootView.findViewById(R.id.SerieDetailRatingBar);
        CheckBox favoriteCheckBox   = (CheckBox) rootView.findViewById(R.id.SerieDetailFavoriteCheckBox);

        titleTextView.setText(mSerie.getTitle());
        plotTextView.setText(mSerie.getPlot());
        // On recupere l'image avec l'url et on l'ajoute à la vue
        Picasso.with(imageImageView.getContext()).load(mSerie.getPoster()).into(imageImageView);

        yearTextView.setText(mSerie.getYear());
        directorTextView.setText(mSerie.getDirector());
        genreTextView.setText(mSerie.getGenre());
        actorsTextView.setText(mSerie.getActors());

        //On gere la note
        String rate = mSerie.getImdbRating() ;
        Float rateFloat = Float.valueOf(rate) / 2 ;
        BigDecimal roundRate = new BigDecimal(rateFloat).setScale(1,BigDecimal.ROUND_HALF_UP);
        Float finalRate = Float.valueOf(String.valueOf(roundRate));
        ratingBar.setStepSize(Float.valueOf("0.1"));
        ratingBar.setRating(finalRate);


        if(mSerie.getFavorite()) {
            favoriteCheckBox.setChecked(!favoriteCheckBox.isChecked());
        }


        favoriteCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                List<Serie> serieList           = AllSerieListSingleton.getInstance().getSerieList();
                List<Serie> BestserieList       = BestSerieListSingleton.getInstance().getSerieList();
                List<Serie> favoriteSerieList   = FavoriteSerieListSingleton.getInstance().getSerieList();

                if (isChecked) {
                    Snackbar.make(rootView, "The series has been added to favorites successfully", Snackbar.LENGTH_LONG)
                            .show();

                    mSerie.setFavorite(true);

                    if(CompareSerieList.ListContainsTitle(serieList, mSerie.getTitle())){
                        int position = CompareSerieList.getPositionByTitle(serieList, mSerie.getTitle());
                        serieList.remove(position);
                        serieList.add(position, mSerie);
                        AllSerieListSingleton.getInstance().setSerieList(serieList);
                    }

                    if(CompareSerieList.ListContainsTitle(BestserieList, mSerie.getTitle())){
                        int position = CompareSerieList.getPositionByTitle(BestserieList, mSerie.getTitle());
                        BestserieList.remove(position);
                        BestserieList.add(position, mSerie);
                        BestSerieListSingleton.getInstance().setSerieList(BestserieList);
                    }

                    if(!CompareSerieList.ListContainsTitle(favoriteSerieList, mSerie.getTitle())){
                        favoriteSerieList.add(mSerie);
                        FavoriteSerieListSingleton.getInstance().setSerieList(favoriteSerieList);
                    }


                } else {
                    Snackbar.make(rootView, "The series has been removed from favorites successfully", Snackbar.LENGTH_LONG)
                            .show();

                    mSerie.setFavorite(false);

                    if(CompareSerieList.ListContainsTitle(serieList, mSerie.getTitle())){
                        int position = CompareSerieList.getPositionByTitle(serieList, mSerie.getTitle());
                        serieList.remove(position);
                        serieList.add(position, mSerie);
                        AllSerieListSingleton.getInstance().setSerieList(serieList);
                    }

                    if(CompareSerieList.ListContainsTitle(BestserieList, mSerie.getTitle())){
                        int position = CompareSerieList.getPositionByTitle(BestserieList, mSerie.getTitle());
                        BestserieList.remove(position);
                        BestserieList.add(position, mSerie);
                        BestSerieListSingleton.getInstance().setSerieList(BestserieList);
                    }

                    if(CompareSerieList.ListContainsTitle(favoriteSerieList, mSerie.getTitle())){
                        int position = CompareSerieList.getPositionByTitle(favoriteSerieList, mSerie.getTitle());
                        favoriteSerieList.remove(position);
                        FavoriteSerieListSingleton.getInstance().setSerieList(favoriteSerieList);
                    }

                }
            }
        });

        return rootView;
    }
}
