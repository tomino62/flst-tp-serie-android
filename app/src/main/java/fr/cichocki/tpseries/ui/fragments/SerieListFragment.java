package fr.cichocki.tpseries.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import fr.cichocki.tpseries.R;
import fr.cichocki.tpseries.adapter.SerieAdapter;
import fr.cichocki.tpseries.model.AllSerieListSingleton;
import fr.cichocki.tpseries.model.BestSerieListSingleton;
import fr.cichocki.tpseries.model.FavoriteSerieListSingleton;
import fr.cichocki.tpseries.model.Serie;
import fr.cichocki.tpseries.util.ItemClickSupport;

public class SerieListFragment extends Fragment {

    private List<Serie> serieList;
    private String mTypeList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_serie_list, container, false);

        mTypeList = (String) getArguments().getSerializable("list");

        if(mTypeList.equalsIgnoreCase("all")){
            serieList = AllSerieListSingleton.getInstance().getSerieList();
        }
        else if(mTypeList.equalsIgnoreCase("best")) {
            serieList = BestSerieListSingleton.getInstance().getSerieList();
        }
        else if(mTypeList.equalsIgnoreCase("favorite")) {
            serieList = FavoriteSerieListSingleton.getInstance().getSerieList();

            TextView emptyTextView = (TextView) rootView.findViewById(R.id.serie_list_empty);
            if(serieList.isEmpty()){
                emptyTextView.setText("No favorite yet ... Add favorites with the star on series details view.");
            }
        }


        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.serie_list);
        recyclerView.setHasFixedSize(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        SerieAdapter adapter = new SerieAdapter(serieList);
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport
                .OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intent = new Intent(getActivity(), SerieDetailActivity.class);
                intent.putExtra("serie", serieList.get(position));
                startActivity(intent);
            }
        });


        return rootView;
    }

    @Override
    public String toString (){

        mTypeList = (String) getArguments().getSerializable("list");

        if(mTypeList.equalsIgnoreCase("all")){
            return "All series";
        }
        else if(mTypeList.equalsIgnoreCase("best")) {
            return "Best rated";
        }
        else if(mTypeList.equalsIgnoreCase("favorite")) {
            return "Your favorites";
        }
        else{
            return "";
        }
    }

}
