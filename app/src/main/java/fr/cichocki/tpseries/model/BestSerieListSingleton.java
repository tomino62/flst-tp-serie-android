package fr.cichocki.tpseries.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tcichock on 18/02/2016.
 */
public class BestSerieListSingleton {

    private static BestSerieListSingleton mInstance = null;

    private static List<Serie> mSerieList = new ArrayList<Serie>();

    private BestSerieListSingleton(){
        mSerieList = new ArrayList<Serie>();
    }

    public static BestSerieListSingleton getInstance(){
        if(mInstance == null)
        {
            mInstance = new BestSerieListSingleton();
        }
        return mInstance;
    }

    public List<Serie> getSerieList(){
        return mSerieList;
    }

    public void setSerieList(List<Serie> value){
        mSerieList = value;
    }

}
