package fr.cichocki.tpseries.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tcichock on 18/02/2016.
 */
public class AllSerieListSingleton {

    private static AllSerieListSingleton mInstance = null;

    private static List<Serie> mSerieList = new ArrayList<Serie>();

    private AllSerieListSingleton(){
        mSerieList = new ArrayList<Serie>();
    }

    public static AllSerieListSingleton getInstance(){
        if(mInstance == null)
        {
            mInstance = new AllSerieListSingleton();
        }
        return mInstance;
    }

    public List<Serie> getSerieList(){
        return mSerieList;
    }

    public void setSerieList(List<Serie> value){
        mSerieList = value;
    }

}
