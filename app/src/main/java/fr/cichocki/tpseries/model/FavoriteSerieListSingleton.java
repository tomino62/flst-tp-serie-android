package fr.cichocki.tpseries.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tcichock on 18/02/2016.
 */
public class FavoriteSerieListSingleton {

    private static FavoriteSerieListSingleton mInstance = null;

    private static List<Serie> mSerieList = new ArrayList<Serie>();

    private FavoriteSerieListSingleton(){
        mSerieList = new ArrayList<Serie>();
    }

    public static FavoriteSerieListSingleton getInstance(){
        if(mInstance == null)
        {
            mInstance = new FavoriteSerieListSingleton();
        }
        return mInstance;
    }

    public List<Serie> getSerieList(){
        return mSerieList;
    }

    public void setSerieList(List<Serie> value){
        mSerieList = value;
    }

}
