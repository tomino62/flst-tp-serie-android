package fr.cichocki.tpseries.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fr.cichocki.tpseries.R;
import fr.cichocki.tpseries.model.Serie;

public class SerieAdapter extends RecyclerView.Adapter<SerieAdapter.ViewHolder> {

    private List<Serie> mSerieList;

    public SerieAdapter(List<Serie> serieList) {
        mSerieList = serieList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.serie_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Serie serie = mSerieList.get(position);

        // On tronque la description si elle fait plus de 100 caracteres
        String plot = "";
        if(serie.getPlot().length() > 100){
            plot = serie.getPlot().substring(0, 100);
            plot += " [...]" ;
        }
        else{
            plot = serie.getPlot() ;
        }

        holder.mTileTxv.setText(serie.getTitle());
        holder.mPlotTxv.setText(plot);
        // On recupere l'image avec l'url et on l'ajoute à la vue
        Picasso.with(holder.mIconImv.getContext()).load(serie.getPoster()).into(holder.mIconImv);
    }

    @Override
    public int getItemCount() {
        return mSerieList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTileTxv;
        private TextView mPlotTxv;
        private ImageView mIconImv;

        public ViewHolder(View v) {
            super(v);
            mTileTxv = (TextView) v.findViewById(R.id.textViewTitle);
            mPlotTxv = (TextView) v.findViewById(R.id.textViewPlot);
            mIconImv = (ImageView) v.findViewById(R.id.icon);
        }
    }

}

