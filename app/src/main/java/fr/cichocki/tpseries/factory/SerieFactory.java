package fr.cichocki.tpseries.factory;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import fr.cichocki.tpseries.model.AllSerieListSingleton;
import fr.cichocki.tpseries.util.HTTPDataHandler;
import fr.cichocki.tpseries.model.Serie;
import fr.cichocki.tpseries.util.SerieRatingCompare;

/**
 * Created by Thomas.
 */
public class SerieFactory {

    private static String urlString;

    private static List<Serie> serieList         = new ArrayList<Serie>();
    private static List<Serie> bestSerieList     = new ArrayList<Serie>();
    private static List<Serie> favoriteSerieList = new ArrayList<Serie>();

    private static String[] seriesNames = {
                                            "The walking dead",
                                            "Game of Thrones",
                                            "Heroes",
                                            "Breaking Bad",
                                            "Borgen",
                                            "Lost",
                                            "24",
                                            "Vikings",
                                            "Once Upon A Time",
                                            "Person Of Interest",
                                            "Homeland",
                                            "X-Files",
                                            "Suits",
                                            "Better Call Saul",
                                            "Prison Break",
                                            "The Big Bang Theory",
                                            "Doctor Who",
                                            "Stargate",
                                            "Stargate Atlantis",
                                            "Battlestar Galactica"
                                    };

    public static List<Serie> getSerieList() {

        for (String name : seriesNames) {

            name        = name.replaceAll(" ", "%20");
            urlString   = "http://www.omdbapi.com/?t=" + name + "&type=series&plot=full&r=json";

            String result = null;

            try {
                result = new ProcessJSON().execute(urlString).get();

                // On obtienbt toute la reponse de la requete dans un objet JSONObject
                JSONObject reader = new JSONObject(result);

                // On recupere les infos voulues
                String title        = reader.getString("Title");
                String plot         = reader.getString("Plot");
                String year         = reader.getString("Year");
                String genre        = reader.getString("Genre");
                String poster       = reader.getString("Poster");
                String actors       = reader.getString("Actors");
                String imdbRating   = reader.getString("imdbRating");
                String director     = reader.getString("Director");

                serieList.add(new Serie(title, plot, year, genre, poster, actors, imdbRating, director));

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return serieList;
    }

    public static List<Serie> getBestSeriesList() {

        serieList = AllSerieListSingleton.getInstance().getSerieList();

        for (Serie serie : serieList) {

            if(Float.valueOf(serie.getImdbRating()) >= 8.75) {
                bestSerieList.add(serie);
            }

        }

        Collections.sort(bestSerieList, new SerieRatingCompare());

        return bestSerieList;
    }

    public static List<Serie> getFavoriteSeriesList() {

        serieList = AllSerieListSingleton.getInstance().getSerieList();

        for (Serie serie : serieList) {

            if(serie.getFavorite()) {
                favoriteSerieList.add(serie);
            }

        }

        return favoriteSerieList;
    }


    private static class ProcessJSON extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... strings){
            String stream = null;
            String urlString = strings[0];

            HTTPDataHandler hh = new HTTPDataHandler();
            stream = hh.GetHTTPData(urlString);


            // Return the data from specified url
            return stream;
        }
    }
}
