THOMAS CICHOCKI
========================

FST - RIZOMM
========================

Android - TP Mes s�ries
--------------

  * L'application n�cessite une connexion internet pour 
    fonctionner correctement (r�cup�ration des informations 
    des s�ries et des images via une api).


  * Pour ajouter une s�rie aux favoris, aller sur le d�tail 
    d�une s�rie et cliquer sur l��toile en haut � gauche. 
    Cette s�rie sera maintenant visible dans la liste des favoris. 
    Cliquer de nouveau sur l��toile pour la retirer des favoris.


  * Les listes de s�ries sont des Singletons. Les favoris seront 
    donc r�initialis�s � chaque red�marrage  de l�application.